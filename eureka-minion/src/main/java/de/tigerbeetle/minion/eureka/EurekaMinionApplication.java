package de.tigerbeetle.minion.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class EurekaMinionApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaMinionApplication.class, args);
	}

}
